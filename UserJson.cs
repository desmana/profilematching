﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deserialize
{
    public class Relation_Partner
    {
        public virtual string id { get; set; }
        public virtual string first_name { get; set; }
        public virtual string last_name { get; set; }
    }

    public class Country
    {
        [JsonProperty("title")]
        public string countryTitle { get; set; }
    }

    public class City
    {
        public virtual int id { get; set; }
        [JsonProperty("title")]
        public virtual string cityTitle { get; set; }
    }

    public class Career
    {
        public virtual int? group_id { get; set; }
        public virtual string company { get; set; }
        public virtual string city_name { get; set; }
        public virtual string from { get; set; }
        public virtual string until { get; set; }
        public virtual string position { get; set; }
    }

    public class Military
    {
        public virtual long? Id { get; set; }
        public virtual string unit { get; set; }
        public virtual int? unit_id { get; set; }
        public virtual int? country_id { get; set; }
        public virtual int? from { get; set; }
        public virtual int? until { get; set; }
    }

    public class University
    {
        public virtual int? id { get; set; }
        public virtual int? city { get; set; }
        public virtual string city_name { get; set; }
        public virtual string name { get; set; }
        public virtual string faculty_name { get; set; }
        public virtual int? graduation { get; set; }
        public virtual string education_form { get; set; }
        public virtual string education_status { get; set; }
        public virtual string chair_name { get; set; }
    }

    public class School
    {
        public virtual string country { get; set; }
        public virtual int? city { get; set; }
        public virtual string city_name { get; set; }
        public virtual string name { get; set; }
        public virtual string year_from { get; set; }
        public virtual string year_to { get; set; }
        public virtual string year_graduated { get; set; }
        public virtual string speciality { get; set; }
        public virtual int? type { get; set; }
        public virtual string type_str { get; set; }
    }

    public class User
    {
        public void getNamesById()
        {
            string uniCityIds = "";
            if (universities.Count > 0)
            {
                for (int i = 0; i < universities.Count; i++)
                {
                    uniCityIds += universities[i].city + ",";
                }
                var uniCityObj = VK.GetCitiesById(uniCityIds);

                Dictionary<int, string> idName = new Dictionary<int, string>();

                for (int i = 0; i < uniCityObj.Count; i++)
                {
                    idName.Add(uniCityObj[i].id, uniCityObj[i].cityTitle);
                }

                for (int i = 0; i < universities.Count; i++)
                {
                    universities[i].city_name = idName[(int)universities[i].id];
                }
            }
            string schlCityIds = "";
            if (schools.Count > 0)
            {
                for (int i = 0; i < schools.Count; i++)
                {
                    schlCityIds += schools[i].city + ",";
                }
                var schoolcityObj = VK.GetCitiesById(schlCityIds);

                Dictionary<int, string> idName = new Dictionary<int, string>();

                for (int i = 0; i < schoolcityObj.Count; i++)
                {
                    idName.Add(schoolcityObj[i].id, schoolcityObj[i].cityTitle);
                }

                for (int i = 0; i < universities.Count; i++)
                {
                    schools[i].city_name = idName[(int)schools[i].city];
                }
            }
        }

        public void refactorXZ()
        {
            if (career != null)
            {
                String str = "";
                foreach (var workplace in career)
                {
                    str += workplace.group_id + " " + workplace.company + " " + workplace.position + " " + workplace.city_name + " " + workplace.from + " " + workplace.until + ",";
                }
                careerList = str;
            }
            careerList = null;

            if (military != null)
            {
                String str = "";
                foreach (var militaryplace in military)
                {
                    str += militaryplace.unit + " " + militaryplace.country_id + " " + militaryplace.from + " " + militaryplace.until + ",";
                }
                militaryList =str;
            }
            militaryList = null;

            if (schools != null)
            {
                String str = "";
                foreach (var school in schools)
                {
                    str += school.name + " " + school.type_str + " " + school.speciality + " " + school.city + " " + school.country + " " + school.year_from + " " + school.year_to + ",";
                }
                schoolList =str;
            }
            schoolList = null;

            if (universities != null)
            {
                String str = "";
                foreach (var university in universities)
                {
                    str += university.name + " " + university.faculty_name + " " + university.chair_name + " " +
                        university.city + " " + university.graduation + ",";
                }
                universitylList = str;
            }
            universitylList = null;
        }

        public virtual string about { get; set; }
        public virtual string bdate { get; set; }
        public virtual string books { get; set; }
        public virtual List<Career> career { get; set; }
        public virtual String careerList { get; set; }
        public virtual City city { get; set; }
        public virtual City cityGenerated { get; set; }
        public virtual Country country { get; set; }
        public virtual string domain { get; set; }
        public virtual string education_form { get; set; }
        public virtual string education_status { get; set; }
        public virtual string faculty_name { get; set; }
        public virtual string first_name { get; set; }
        public virtual List<User> friendList { get; set; }
        public virtual string games { get; set; }
        public virtual int? graduation { get; set; }
        public virtual string home_town { get; set; }
        public virtual string hometownGenerated { get; set; }
        public virtual int Id { get; set; }
        [JsonProperty("id")]
        public virtual string idVkOk { get; set; }
        public virtual string interests { get; set; }
        public virtual string last_name { get; set; }
        public virtual string maiden_name { get; set; }
        public virtual List<Military> military { get; set; }
        public virtual String militaryList { get; set; }
        public virtual string movies { get; set; }
        public virtual string music { get; set; }
        public virtual string nickname { get; set; }
        public virtual string relation { get; set; }
        public virtual List<School> schools { get; set; }
        public virtual String schoolList { get; set; }
        public virtual string screen_name { get; set; }
        public virtual string sex { get; set; }
        public virtual string status { get; set; }
        public virtual int? university { get; set; }
        public virtual string university_name { get; set; }
        public virtual List<University> universities { get; set; }
        public virtual String universitylList { get; set; }
    }

    public class UserList
    {
        [JsonProperty("response")]
        public List<User> user { get; set; }
    }

    public partial class CityList
    {
        [JsonProperty("response")]
        public List<City> city { get; set; }
    }
}