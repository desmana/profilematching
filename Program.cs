﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;


namespace deserialize
{
    public class Program
    {
        static void Main(string[] args)
        {
            User userOK = OK.GetProfileInfo("568265951909");
            User userVK = VK.GetProfileInfo("244568440");

            DataRecoverer.DataRecover(ref userOK);
            DataRecoverer.DataRecover(ref userVK);

            DataPreprocesser.DataPreprocess(ref userOK);
            DataPreprocesser.DataPreprocess(ref userVK);

            DataUnificatior.DataUnificate(ref userOK);
            DataUnificatior.DataUnificate(ref userVK);

            var compTable = Matching.CompareProfiles(userOK, userVK);

            Console.ReadKey();
        }
    }
}
