﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace deserialize
{
    public static class DataPreprocesser
    {
        private static List<string> badWords = null;

        public static void DataPreprocess(ref User user)
        {
            user.about = TextPreProcess(user.about);
            user.books = TextPreProcess(user.books);
            // user.career 
            user.domain = user.domain?.ToLower();
            user.first_name = WordPreProcess(user.first_name);
            user.first_name = BackTransliteration(user.first_name);
            user.games = TextPreProcess(user.domain);
            user.home_town = WordPreProcess(user.home_town);
            user.interests = TextPreProcess(user.interests);
            user.last_name = BackTransliteration(user.last_name);
            user.last_name = WordPreProcess(user.last_name);
            user.maiden_name = WordPreProcess(user.maiden_name);
            user.maiden_name = BackTransliteration(user.maiden_name);
            //  user.military
            user.movies = TextPreProcess(user.movies);
            user.music = TextPreProcess(user.music);
            user.nickname = user.nickname?.ToLower();
            user.relation = user.relation?.ToLower();
            //   user.schools = TextDataPreProcess(user.schools);
            user.screen_name = user.screen_name?.ToLower();
            user.status = TextPreProcess(user.status);
             //   user.universities
        }

        private static string TextPreProcess(string str)
        {
            if (str == null)
                return null;
            str = RemoveBadChars(str);
            str = str?.ToLower();
            str = RemoveBadWords(str);
            return str;
        }

        private static string WordPreProcess(string str)
        {
            if (str == null)
                return null;
            str = RemoveBadChars(str);
            str = str?.ToLower();
            return str;
        }

        private static string BackTransliteration(string name)
        {
            if (name == null)
                return null;
            Regex r = new Regex("[a-zA-Z]");
            if (r.IsMatch(name))
            {
                string[] lat_low = { "shch", "shh", "ch", "sh", "yu", "ju", "ya", "ja", "ts", "kh", "yo", "jo", "zh", "a", "b", "v", "g", "d", "e", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "\"", "y", "'", "e" };
                string[] rus_low = { "щ", "щ", "ч", "ш", "ю", "ю", "я", "я", "ц", "х", "ё", "ё", "ж", "а", "б", "в", "г", "д", "е", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ъ", "ы", "ь", "э" };
                for (int i = 0; i < 39; i++)
                {
                    name = name.Replace(lat_low[i], rus_low[i]);
                }
            }
            return name;
        }

        private static string RemoveBadWords(string str)
        {
            if (str == null)
                return null;
            if (badWords == null)
            {
                badWords = new List<string>();
                var exePath = AppDomain.CurrentDomain.BaseDirectory;
                var path = Path.Combine(exePath, "badWords.txt");
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        badWords.Add(line);
                    }
                }
            }
            foreach (var badWord in badWords)
            {
                RemoveWords(str, badWord);
            }
            return str;
        }

        private static string RemoveWords(string str, string  badWord) //TODO
        {
            if (str == null)
                return null;
            Regex reg = new Regex($"\b{badWord}");
            return reg.Replace(str, string.Empty);
        }

        private static string RemoveBadChars(string str)
        {
            if (str == null)
                return null;
            Regex reg = new Regex("[,.;:\"']");
            str = reg.Replace(str, " ");
            reg = new Regex("[^а-яА-Я' ]");
            return reg.Replace(str, string.Empty);
        }
    }

    public static class DataUnificatior
    {
        private static Dictionary<string, int?> sexMapping = null;

        public static void DataUnificate(ref User user)
        {
            user.relation = RelationshipStatusUnificate(user.relation);
            user.sex = SexUnificate(user.sex);
            user.bdate = BDateUnificate(user.bdate);
            user.first_name = NameUnificate(user.first_name);
        }

        public static string RelationshipStatusUnificate(string str) 
        {
            if (sexMapping == null)
            {
                sexMapping = new Dictionary<string, int?>();
                
                var exePath = AppDomain.CurrentDomain.BaseDirectory;
                var path = Path.Combine(exePath, "RelationshipStatusMapping.txt");
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null && line != "")
                    {
                        string[] mappingSplitedByComma = line.Split(',');
                        var key = mappingSplitedByComma[0].Trim();
                        int? value;
                        if (mappingSplitedByComma[1].Trim() == "null")
                        {
                            value = null;
                        }
                        else
                        {
                            value = int.Parse(mappingSplitedByComma[1]);
                        }
                        sexMapping.Add(key, value);
                    }
                }
                
            }
            return sexMapping[str].ToString();
        }

        public static string SexUnificate(string sex)
        {
            if (sex == "female")
                return "1";
            else
                return "2";
        }

        public static string BDateUnificate(string bDate)
        {
            var splitedBDate = bDate.Split(' ');

            if (splitedBDate.Length > 1)
            {

                int month = 0;

                switch (splitedBDate[1])
                {
                    case "января":
                        month = 1;
                        break;
                    case "февраля":
                        month = 2;
                        break;
                    case "марта":
                        month = 3;
                        break;
                    case "апреля":
                        month = 4;
                        break;
                    case "мая":
                        month = 5;
                        break;
                    case "июня":
                        month = 6;
                        break;
                    case "июля":
                        month = 7;
                        break;
                    case "августа":
                        month = 8;
                        break;
                    case "сентября":
                        month = 9;
                        break;
                    case "октября":
                        month = 10;
                        break;
                    case "ноября":
                        month = 11;
                        break;
                    case "декабря":
                        month = 12;
                        break;
                }
                bDate = splitedBDate[0] + '.' + month;
                if (splitedBDate.Length > 2)
                {
                    bDate += '.'+splitedBDate[2];
                }
            }
            return bDate;
        }

        public static string NameUnificate(string str) //TODO
        {
            return str;
        }
    }

    public static class DataRecoverer
    {
        public static void DataRecover(ref User user)
        {
            CurrentCityRecover(ref user);
            HomeTownRecover(ref user);
        }


        public static void CurrentCityRecover(ref User user)
        {
                var cities = new Dictionary<string, int>();
            if (user.friendList != null)
            {
                foreach (var friend in user.friendList)
                {
                    if (friend.city != null)
                    {
                        if (!cities.ContainsKey(friend.city.cityTitle))
                            cities.Add(friend.city.cityTitle, 1);
                        else
                            cities[friend.city.cityTitle]++;
                    }
                }
                string max_city = null;
                int max = 0;
                foreach (var city in cities)
                {
                    if (city.Value > max)
                    {
                        max_city = city.Key;
                        max = city.Value;
                    }
                }
                user.cityGenerated = new City() { cityTitle = max_city };
            }
        }

        public static void HomeTownRecover(ref User user)
        {

                var cities = new Dictionary<string, int>();
            if (user.friendList != null)
            {
                foreach (var friend in user.friendList)
                {
                    if (friend.home_town != null)
                    {
                        if (!cities.ContainsKey(friend.home_town))
                            cities.Add(friend.home_town, 1);
                        else
                            cities[friend.home_town]++;
                    }
                }
                string max_city = null;
                int max = 0;
                foreach (var city in cities)
                {
                    if (city.Value > max)
                    {
                        max_city = city.Key;
                        max = city.Value;
                    }
                }
                user.hometownGenerated = max_city;
            }
            
        }
    }
}
