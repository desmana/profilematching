﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deserialize
{
    public static class VK
    {
        private static string accessTocken;

        public static User GetProfileInfo(string id)
        {
            if (accessTocken == null)
            {
                var exePath = AppDomain.CurrentDomain.BaseDirectory;
                var path = Path.Combine(exePath, "AccessTocken.txt");

                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    accessTocken = sr.ReadLine();
                }
            }
            var b = GetMethods.GET($"https://api.vk.com/method/users.get?user_ids={id}&fields=first_name_" +
                "{nom}" +
                ",about,bdate,books,career,country,domain,city," +
                  "education,games,home_town,interests,last_name_{nom},maiden_name,military,movies,music,nickname,personal," +
                  $"relation,schools,screen_name,sex,status,universities, " +
                  $"&access_token={accessTocken}&v=5.85");

            var userObj = JsonConvert.DeserializeObject<UserList>(b);
          //  userObj.user.First().refactorXZ();
            var user = userObj.user.First();
            user.getNamesById();

            return user;
        }

        public static List <City> GetCitiesById(string CityIds)
        {
            var CityJson = GetMethods.GET($"https://api.vk.com/method/database.getCitiesById?city_ids={CityIds}&access_token={accessTocken}&v=5.85");
            var CityObj = JsonConvert.DeserializeObject<CityList>(CityJson);
            return CityObj.city;
        }

        public static void GetFriends(ref User user)
        {
            string link = $"https://m.ok.ru/profile/{user.idVkOk}/friends";
            user.friendList = new List<User>();

            var b = GetMethods.GET($"https://api.vk.com/method/users.get?user_ids={user.idVkOk}&fields=first_name_" +
                "{nom}" +
                ",about,bdate,books,career,country,domain,city," +
                  "education,games,home_town,interests,last_name_{nom},maiden_name,military,movies,music,nickname,personal," +
                  $"relation,schools,screen_name,sex,status,universities, " +
                  $"&access_token={accessTocken}&v=5.85"); 

            var friendsObj = JsonConvert.DeserializeObject<UserList>(b);
            user.friendList = friendsObj.user;
        }
    }
}
