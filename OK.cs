﻿using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace deserialize
{
    public static class OK
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="selectorOfName"></param>
        public static void GetName(ref User user, IElement selectorOfName)
        {
            var fullname = selectorOfName.Text();
            user.first_name = fullname.Substring(0, fullname.IndexOf(' '));
            var lastName = fullname.Remove(0, fullname.IndexOf(' '));

            if (lastName.Contains('('))
            {
                user.last_name = lastName.Substring(0, lastName.IndexOf('('));
                user.maiden_name = lastName.Substring(lastName.IndexOf('(') + 1, lastName.IndexOf(')') - lastName.IndexOf('(') - 1);
            }
            else
            {
                user.last_name = lastName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="now"></param>
        public static void GetCareer(ref User user, IElement now)
        {
            user.career = new List<Career>();
            var careerNext = now.NextElementSibling;
            var careerSelect = careerNext.QuerySelectorAll("div.user-profile_i_value");
            foreach (var item in careerSelect)
            {
                var companyCountCity = item.QuerySelector("span").GetAttribute("title");
                var splitCompanyByComma = companyCountCity.Split(',');
                var yearPosition = item.QuerySelector("span.darkgray").Text();
                var splitYearByComma = yearPosition.Split(',');
                var splitBySpace = splitYearByComma[0].Split(' ');
                var career = new Career
                {
                    company = splitCompanyByComma[0],
                    city_name = splitCompanyByComma[1],
                    from = splitBySpace[1],
                };
                if (splitYearByComma.Length == 2)
                {
                    career.position = splitYearByComma[1];
                }
                if (splitBySpace.Length == 4)
                {
                    career.until = splitBySpace[3];
                }
                user.career.Add(career);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="now"></param>
        public static void GetPersonal(ref User user, IElement now)
        {
            if (Convert.ToBoolean(now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml.Contains("Родил")))
            {
                if (Convert.ToBoolean(now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml.Contains("Родилась")))
                {
                    user.sex = "2";
                }
                else
                {
                    user.sex = "1";
                }
                user.bdate = now.QuerySelector("div.user-profile_i_value").InnerHtml;
            }
            if (now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml == "Место рождения")
            {
                user.home_town = now.QuerySelector("div.user-profile_i_value").InnerHtml;
            }
            if (now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml == "Живет в")
            {
                user.city = new City();
                user.city.cityTitle = now.QuerySelector("div.user-profile_i_value").InnerHtml;
            }
            if (now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml == "В браке" ||
                now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml == "В отношениях" ||
                now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml.Contains("Разведен") == true ||
                now.QuerySelector("span.user-profile_i_t_inner")?.InnerHtml.Contains("Открыт") == true)
            {
                user.relation = now.QuerySelector("span.user-profile_i_t_inner").InnerHtml;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="now"></param>
        public static void GetSchool(ref User user, IElement now)
        {
            user.schools = new List<School>();
            var schoolNext = now.NextElementSibling;
            var schoolSelect = schoolNext.QuerySelectorAll("div.user-profile_i_value");
            foreach (var item in schoolSelect)
            {
                var schoolCountryCity = item.QuerySelector("span").GetAttribute("title");
                var splitSchoolByComma = schoolCountryCity.Split(',');
                var yearFaculty = item.QuerySelector("span.darkgray").Text();
                var splitYearByComma = yearFaculty.Split(' ');
                var school = new School
                {
                    name = splitSchoolByComma[0],
                    year_from = splitYearByComma[1]
                };
                if (splitSchoolByComma.Length == 4)
                {
                    school.speciality = splitSchoolByComma[1];
                    school.city_name = splitSchoolByComma[2];
                    school.country = splitSchoolByComma[3];
                }
                else
                {
                    school.city_name = splitSchoolByComma[1];
                    school.country = splitSchoolByComma[2];
                }
                if (splitYearByComma.Length == 4)
                {
                    school.year_to = splitYearByComma[3];
                }
                user.schools.Add(school);
            }
        }

        /// <summary>
        /// Метод, который агрегирует информацию с личной страницы пользователя «Одноклассники» 
        /// </summary>
        /// <param name="user"></param>
        public static User GetProfileInfo(string Id)
        {
            string page;
            using (WebClient web = new WebClient())
            {
                web.Headers["user-agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" +
                                            "Chrome/70.0.3538.110 Safari/537.36";
                web.Headers["cache-control"] = "max-age=0";
                web.Headers["accept-language"] = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
                web.Headers["accept"] = "text/html,application/xhtml+xml,application/xml;q = 0.9,image/webp,image/apng,*/*;q=0.8";
                web.Headers["scheme"] = "https";
                web.Headers["method"] = "GET";
                web.Headers["content-type"] = "text/html; charset=utf-8";
                web.Headers["upgrade-insecure-requests"] = "1";
                page = web.DownloadString($"https://ok.ru/profile/{Id}/about");
            }
            User user = new User();

            var parser = new HtmlParser();
            var document = parser.ParseDocument(page);

            var selectorOfName = document.QuerySelector("div.compact-profile");

            GetName(ref user, selectorOfName);

            var selector = document.QuerySelector("div#mainContentContentColumn");
            var selectorPersonal = selector.QuerySelectorAll("div.user-profile_i");
            foreach (var now in selectorPersonal)
            {
                GetPersonal(ref user, now);
            }

            var selectorLists = selector.QuerySelectorAll("div.user-profile_group_h");
            foreach (var now in selectorLists)
            {
                if (now.Text() == "Карьера")
                {
                    GetCareer(ref user, now);
                };
                if (now.Text() == "Учеба")
                {
                    GetSchool(ref user, now);
                };
            }
       /*     var sessionFactory = DB.CreateSessionFactoryForUser();
            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {

                    session.SaveOrUpdate(user);
                    transaction.Commit();
                }
            }*/
            return user;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public static void GetFriends(ref User user)
        {
            string link = $"https://m.ok.ru/profile/{user.idVkOk}/friends";
            user.friendList = new List<User>();

            List<User> friendList = new List<User>();

            while (true)
            {
                string page;

                using (WebClient web = new WebClient())
                {
                    web.Headers["user-agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" +
                                                   "Chrome/70.0.3538.110 Safari/537.36";
                    web.Headers["cache-control"] = "max-age=0";
                    web.Headers["accept-language"] = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6";
                    web.Headers["accept"] = "text/html,application/xhtml+xml,application/xml;q = 0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3";
                    web.Headers["scheme"] = "https";
                    web.Headers["method"] = "GET";
                    web.Headers["content-type"] = "text/html; charset=utf-8";
                    web.Headers["upgrade-insecure-requests"] = "1";
                    page = web.DownloadString(link);
                }

                var parser = new HtmlParser();
                var document = parser.ParseDocument(page);

                var selectorOfFriends = document.QuerySelectorAll("div.wide-user_name");

                if (selectorOfFriends.Length == 0)
                {
                    break;
                }

                foreach (var friendInfo in selectorOfFriends)
                {
                    var friendLink = friendInfo.QuerySelector("a")?.GetAttribute("href");
                    var id = friendLink.Substring(friendLink.IndexOf("e/") + 2, friendLink.IndexOf('?') - friendLink.IndexOf("e/")-2);
                    friendList.Add(new User() { idVkOk = id });
                }

                var loadMoreFriendsLinkPartial = document.QuerySelector("a.navlnk").GetAttribute("href");
                var loadMoreFriendsLinkFull = @"https://m.ok.ru" + loadMoreFriendsLinkPartial;

                link = loadMoreFriendsLinkFull;
            }

            foreach (var friend in friendList)
            {
                user.friendList.Add(GetProfileInfo(friend.idVkOk));
            }
        }
    }
}
