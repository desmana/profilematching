﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deserialize
{
    public class MatchingTable
    {
        public virtual double? about { get; set; }
        public virtual double? bdate { get; set; }
        public virtual double? books { get; set; }
        public virtual double? career { get; set; }
        public virtual double? city { get; set; }
        public virtual double? cityGenerated { get; set; }
        public virtual double? country { get; set; }
        public virtual double? domain { get; set; }
        public virtual double? first_name { get; set; }
        public virtual double? games { get; set; }
        public virtual double? home_town { get; set; }
        public virtual double? hometownGenerated { get; set; }
        public virtual double Id { get; set; } //МОЖНО ЛИ БЕЗ ЭТОГО? 
        public virtual string idVk { get; set; }
        public virtual string idOk { get; set; }
        public virtual double? interests { get; set; }
        public virtual double? last_name { get; set; }
        public virtual double? maiden_name { get; set; }
        public virtual double? military { get; set; }
        public virtual double? movies { get; set; }
        public virtual double? music { get; set; }
        public virtual double? nickname { get; set; }
        public virtual double? relation { get; set; }
        public virtual double? schools { get; set; }
        public virtual double? screen_name { get; set; }
        public virtual double? sex { get; set; }
        public virtual double? status { get; set; }
        public virtual double? universities { get; set; }
    }
}
