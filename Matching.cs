﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace deserialize
{
    public static class Matching
    {
        public static MatchingTable CompareProfiles(User userOK, User userVK)
        {

            MatchingTable comparisonTable = new MatchingTable()
            {
                about = ArraysMatch(SplitDistinct(userOK.about), SplitDistinct(userVK.about), FuzzyMatch),
                bdate = ExactMatch(userOK.bdate, userVK.bdate),
                books = ArraysMatch(SplitDistinct(userOK.books), SplitDistinct(userVK.books), FuzzyMatch),
                career = ArraysMatch(userOK.career, userVK.career, CareerMatch),
                city = YandexMatch(userOK.city?.cityTitle, userVK.city?.cityTitle, "город"),
                cityGenerated = YandexMatch(userOK.cityGenerated?.cityTitle, userVK.cityGenerated?.cityTitle, "город"),
                country = YandexMatch(userOK.country?.countryTitle, userVK.country?.countryTitle, "страна"),
                domain = FuzzyMatch(userOK.domain, userVK.domain),
                first_name = ExactMatch(userOK.first_name, userVK.first_name),
                games = ArraysMatch(SplitDistinct(userOK.games), SplitDistinct(userVK.games), FuzzyMatch),
                hometownGenerated = YandexMatch(userOK.hometownGenerated, userVK.hometownGenerated, "город"),
                home_town = YandexMatch(userOK.home_town, userVK.home_town, "город"),
                idOk = userOK.idVkOk,
                idVk = userVK.idVkOk,
                interests = ArraysMatch(SplitDistinct(userOK.interests), SplitDistinct(userVK.interests), FuzzyMatch),
                last_name = FuzzyMatch(userOK.last_name, userVK.last_name),
                maiden_name = FuzzyMatch(userOK.maiden_name, userVK.maiden_name),
            //    military,
                movies = ArraysMatch(SplitDistinct(userOK.movies), SplitDistinct(userVK.movies), FuzzyMatch),
                music = ArraysMatch(SplitDistinct(userOK.music), SplitDistinct(userVK.music), FuzzyMatch),
                relation = ExactMatch(userOK.relation, userVK.relation),
              //  schools = ,
                screen_name = FuzzyMatch(userOK.screen_name, userVK.screen_name),
                sex = ExactMatch(userOK.sex, userVK.sex),
                status = ArraysMatch(SplitDistinct(userOK.status), SplitDistinct(userVK.status), FuzzyMatch),
                universities = ArraysMatch(userOK.universities, userVK.universities, EducationMatch)
            };
            return comparisonTable;
        }

        public static List<string> SplitDistinct( string value)
        {
            return value?.Split(' ').Distinct().ToList();
        }

        public static int? ExactMatch(string valueOk, string valueVK) //TODO проверку на null
        {
            if (valueOk == null || valueVK == null)
                return null;
            if (valueOk.Equals(valueVK))
                return 1;
            else
                return 0;
        }

        public static int? ExactMatch(int? valueOk, int? valueVK) //TODO переделать всё в стринги и это будет не надо
        {
            if (valueOk == null || valueVK == null)
                return null;
            if (valueOk == valueVK)
                return 1;
            else
                return 0;
        }

        public static double? FuzzyMatch(string valueOK, string valueVK) 
        {
            if (valueOK == null || valueVK == null)
                return null;
            return JaroWinkler.RateSimilarity(valueVK, valueOK);
        }

        public static int? YandexMatch(string valueOK, string valueVK, string searchPhrase)
        {
            if (valueOK == null || valueVK == null)
                return null;
            int match = 0;

            var resultOK =  GetMethods.GETXML(@"https://yandex.com/search/xml?l10n=en&user=desmanaw&key=03.876912340:ec29db3ec1614010b7cfcd0fdf3de583"
                                   + $"&query={valueOK}%20{searchPhrase}");
            var resultVK = GetMethods.GETXML(@"https://yandex.com/search/xml?l10n=en&user=desmanaw&key=03.876912340:ec29db3ec1614010b7cfcd0fdf3de583"
                                  + $"&query={valueVK}%20{searchPhrase}");

            var rootOK = resultOK.Root;
            var urlsOK = rootOK.XPathSelectElements("//url");

            var rootVK = resultVK.Root;
            var urlsVK = rootOK.XPathSelectElements("//url");

            foreach (var urlOK in urlsOK)
            {
                foreach (var urlVK in urlsVK)
                {
                    if (XNode.DeepEquals(urlOK, urlVK))
                    {
                        match++;
                    }
                }
            }

            if (match >= 4)
                return 1;
            else return 0;
            /*using (StreamWriter sr = new StreamWriter($"YandexXMLtest.txt"))
            {
                sr.Write(a);
            }*/
        }

        public class elementsMatchingResult : IComparable
        {
            public elementsMatchingResult(double comparisonResult, int i, int j)
            {
                result = comparisonResult;
                numberOfFirstWord = i;
                numberOfSecondWord = j;
            }
            public double result;
            public int numberOfFirstWord;
            public int numberOfSecondWord;

            public int CompareTo(object obj)
            {
                elementsMatchingResult element2 = obj as elementsMatchingResult;
                return this.result.CompareTo(element2.result);
            }
        }

        public static double? CareerMatch(Career valueOK, Career valueVK)
        {
            double matchingResult = 0; 
            if (valueOK.company != null && valueVK.company != null)
            {
                matchingResult += (double)FuzzyMatch(valueOK.company, valueVK.company);
            }
            if (valueOK.city_name != null && valueVK.city_name != null)
            {
                matchingResult += (double)YandexMatch(valueOK.city_name, valueVK.city_name, "город");
            }
            if (valueOK.from!= null && valueVK.from != null)
            {
                matchingResult += (double)ExactMatch(valueOK.from, valueVK.from);
            }
            if (valueOK.until != null && valueVK.until != null)
            {
                matchingResult += (double)ExactMatch(valueOK.until, valueVK.until);
            }
            if (valueOK.position != null && valueVK.position != null)
            {
                matchingResult += (double)FuzzyMatch(valueOK.position, valueVK.position);
            }
            return matchingResult/5;
        }

        public static double? EducationMatch(University valueOK, University valueVK)
        {
            double matchingResult = 0;
            if (valueOK.chair_name != null && valueVK.chair_name != null) 
            {
                matchingResult += (double)FuzzyMatch(valueOK.chair_name, valueVK.chair_name);
            }
            if (valueOK.city_name != null && valueVK.city_name!= null)
            {
                matchingResult += (double)YandexMatch(valueOK.city_name, valueVK.city_name, "город");
            } 
            if (valueOK.education_form != null && valueVK.education_form != null)
            {
                matchingResult += (double)FuzzyMatch(valueOK.education_form, valueVK.education_form);
            }
            if (valueOK.education_status != null && valueVK.education_status != null)
            {
                matchingResult += (double)ExactMatch(valueOK.education_status, valueVK.education_status);
            }
            if (valueOK.faculty_name != null && valueVK.faculty_name != null)
            {
                matchingResult += (double)FuzzyMatch(valueOK.faculty_name, valueVK.faculty_name); //химфак химический факультет?
            }
            return matchingResult / 5;
        }

        public static double? ArraysMatch <T>(List<T> array1, List<T> array2, Func<T,T, double?> compare) //с округлением????????
        {
            if (array1 == null || array2 == null)
                return null;
            List<int> indexesOfMatchedWords = new List<int>();
            int maxSize = Math.Max(array1.Count(), array2.Count());
            int matchedWords = 0;
            SortedSet<elementsMatchingResult> comparisonResultForEachField = new SortedSet<elementsMatchingResult>();
            for (int i =0; i< array1.Count(); i++)
                for (int j= 0; j< array2.Count(); j++)
                {
                   comparisonResultForEachField.Add(new elementsMatchingResult((double)compare(array1[i], array2[j]), i, j));
                }
            foreach (var result in comparisonResultForEachField)
            {
                if (result.result < 0.6)
                    break;
                if (indexesOfMatchedWords.Contains(result.numberOfFirstWord) == false && indexesOfMatchedWords.Contains(result.numberOfSecondWord) == false)
                {
                    matchedWords++;
                    indexesOfMatchedWords.Add(result.numberOfFirstWord);
                    indexesOfMatchedWords.Add(result.numberOfSecondWord);
                }
            }
            return matchedWords / maxSize;
        }
    }
}
