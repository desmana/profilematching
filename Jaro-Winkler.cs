﻿using System;
using System.Text;

namespace deserialize
{
    public static class JaroWinkler
    {
        private const double defaultMismatchScore = 0.0;
        private const double defaultMatchScore = 1.0;

        /// <summary>
        /// Gets the similarity between two strings by using the Jaro-Winkler algorithm.
        /// A value of 1 means perfect match. A value of zero represents an absolute no match
        /// </summary>
        /// <param name="_firstWord"></param>
        /// <param name="_secondWord"></param>
        /// <returns>a value between 0-1 of the similarity</returns>
        /// 
        public static double RateSimilarity(string _firstWord, string _secondWord)
        {
            if ((_firstWord != null) && (_secondWord != null))
            {
                if (_firstWord == _secondWord)
                    //return (SqlDouble)defaultMatchScore;
                    return defaultMatchScore;
                else
                {
                    // Get half the length of the string rounded up - (this is the distance used for acceptable transpositions)
                    int halfLength = Math.Min(_firstWord.Length, _secondWord.Length) / 2 + 1;

                    // Get common characters
                    StringBuilder common1 = GetCommonCharacters(_firstWord, _secondWord, halfLength);
                    int commonMatches = common1.Length;

                    // Check for zero in common
                    if (commonMatches == 0)
                        //return (SqlDouble)defaultMismatchScore;
                        return defaultMismatchScore;

                    StringBuilder common2 = GetCommonCharacters(_secondWord, _firstWord, halfLength);

                    // Check for same length common strings returning 0 if is not the same
                    if (commonMatches != common2.Length)
                        //return (SqlDouble)defaultMismatchScore;
                        return defaultMismatchScore;

                    // Get the number of transpositions
                    int transpositions = 0;
                    for (int i = 0; i < commonMatches; i++)
                    {
                        if (common1[i] != common2[i])
                            transpositions++;
                    }

                    int j = 0;
                    j += 1;

                    // Calculate Jaro metric
                    transpositions /= 2;
                    double jaroMetric = commonMatches / (3.0 * _firstWord.Length) + commonMatches / (3.0 * _secondWord.Length) + (commonMatches - transpositions) / (3.0 * commonMatches);
                    //return (SqlDouble)jaroMetric;
                    return jaroMetric;
                }
            }

            //return (SqlDouble)defaultMismatchScore;
            return defaultMismatchScore;
        }

        /// <summary>
        /// Returns a string buffer of characters from string1 within string2 if they are of a given
        /// distance seperation from the position in string1.
        /// </summary>
        /// <param name="firstWord">string one</param>
        /// <param name="secondWord">string two</param>
        /// <param name="separationDistance">separation distance</param>
        /// <returns>A string buffer of characters from string1 within string2 if they are of a given
        /// distance seperation from the position in string1</returns>
        private static StringBuilder GetCommonCharacters(string firstWord, string secondWord, int separationDistance)
        {
            if ((firstWord != null) && (secondWord != null))
            {
                StringBuilder returnCommons = new StringBuilder(20);
                StringBuilder copy = new StringBuilder(secondWord);
                int firstWordLength = firstWord.Length;
                int secondWordLength = secondWord.Length;

                for (int i = 0; i < firstWordLength; i++)
                {
                    char character = firstWord[i];
                    bool found = false;

                    for (int j = Math.Max(0, i - separationDistance); !found && j < Math.Min(i + separationDistance, secondWordLength); j++)
                    {
                        if (copy[j] == character)
                        {
                            found = true;
                            returnCommons.Append(character);
                            copy[j] = '#';
                        }
                    }
                }
                return returnCommons;
            }
            return null;
        }
    }
}
