﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace deserialize
{
    public class UserJsonMapping : ClassMap<User>
    {
        UserJsonMapping()
        {
            Id(u => u.Id).GeneratedBy.Identity();
            Map(u => u.about);
            Map(u => u.bdate);
            Map(u => u.books);
            Map(u => u.careerList);
            Component(u => u.city, m => 
            {
                m.Map(u => u.cityTitle);
            });
            Map(u => u.cityGenerated);
            Component(u => u.country, m =>
            {
                m.Map(u => u.countryTitle);
            });
            Map(u => u.domain);
            Map(u => u.education_form);
            Map(u => u.education_status);
            Map(u => u.faculty_name);
            Map(u => u.first_name);
            Map(u => u.games);
            Map(u => u.graduation);
            Map(u => u.home_town);
            Map(u => u.hometownGenerated);
            Map(u => u.idVkOk);
            Map(u => u.interests);
            Map(u => u.last_name);
            Map(u => u.maiden_name);
            Map(u => u.militaryList);
            Map(u => u.movies);
            Map(u => u.music);
            Map(u => u.nickname);
            Map(u => u.relation);
            Map(u => u.schoolList);
            Map(u => u.screen_name);
            Map(u => u.sex);
            Map(u => u.status);
            Map(u => u.university);
            Map(u => u.universitylList);
            Map(u => u.university_name);
        }
    }
}
